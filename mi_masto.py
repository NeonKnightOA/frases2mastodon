#!/usr/bin/env python3
#
# mi_masto
# Librería con mis funciones de acceso a Mastodon
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
MASTODON_MAX = 500              # Máximo número de caracteres admitido
NUM_MENSA = 20                  # Número de notificaciones/mensajes a comprobar por defecto
T_PROCESO = 10                  # Tiempo aproximado que se tarda en procesar una solicitud (en segundos por exceso)

# LIBRERIAS
from datetime import datetime, timedelta
from dateutil import tz
from lxml import html
from mastodon import Mastodon
from mi_tiempo import mi_hora, a_zona
import configparser
import magic
import os.path as path
import requests
import time
import sys

# FUNCIONES
def _datos_conex( arg_inst_url, arg_app_token ):
    # Leer datos de conexion a Mastodon
    inst_url=''
    app_token=''
    if arg_inst_url:
        inst_url = arg_inst_url
    if arg_app_token:
        app_token = arg_app_token
    if not ( inst_url or app_token ):
        print( 'Faltan los datos de conexión' )
        sys.exit()
    mastodon = Mastodon( access_token = app_token, api_base_url = inst_url )
    print( 'Mi_masto() => Conectado', mastodon.account_verify_credentials()['username'] )
    return mastodon

class Mi_masto():
    def __init__( self, inst_url, app_token, ruta_tmp = '/tmp/' ):
        self.mastodon = _datos_conex( inst_url, app_token )
        self.tmp = ruta_tmp
        if self.tmp[len( self.tmp ) - 1 : len( self.tmp )] != '/':
            self.tmp = self.tmp + '/'

    def masto_max( self ):
        return MASTODON_MAX

    def _nom_fich_tmp( self ):
        # Generar un nombre de fichero temporal para almacenar el último toot procesado
        cuenta = self.mastodon.account_verify_credentials()
        id_cuenta = str( cuenta['id'] )
        nombre_fichero = 'tmp' + id_cuenta + '.ini'
        nombre_fichero = self.tmp + nombre_fichero
        return nombre_fichero

    def _existe_tmp( self ):
        if path.exists( self._nom_fich_tmp() ):
            salida = True
        else:
            salida = False
        return salida

    def _guardar_tmp( self, fecha ):
        # tzona = tz.gettz( 'Europe/Madrid' )
        # mi_fecha = fecha.astimezone( tzona )
        mi_fecha = fecha

        parser = configparser.ConfigParser()
        parser['DEFAULT']['ult_fecha'] = str( self._format_fecha( mi_fecha ) )
        with open( self._nom_fich_tmp(), 'w') as fichero:
            parser.write( fichero )
            fichero.close()

    def _cad2fecha( self, cadena ):
        return datetime.strptime( cadena, '%Y-%m-%d %H:%M:%S.%f' )

    def _leer_t_tmp( self ):
        parser = configparser.ConfigParser()
        parser.read( self._nom_fich_tmp() )
        salida = self._cad2fecha( parser['DEFAULT']['ult_fecha'] )
        return salida

    def _format_fecha( self, fecha ):
        return( fecha.strftime( '%Y-%m-%d %H:%M:%S.%f' ) )

    def comprueba_notif( self, delta_t = 0.5, num_notif = NUM_MENSA ):
        cuenta = self.mastodon.account_verify_credentials()
        # print( 'Comprobando notificaciones de ', self.mi_nombre() , '...' )
        notificaciones = self.mastodon.notifications( limit = num_notif )
        
        hora = mi_hora()
        # print( 'hora = ', hora )
        horaf = self._format_fecha( hora )
        # print( 'horaf = ', horaf )
        t_compara = self._format_fecha( hora - timedelta( seconds = delta_t + T_PROCESO ) )
        # print( 't_compara = ', t_compara )
        if not( self._existe_tmp() ):
            self._guardar_tmp( hora )
        else:
            t_guardado = self._format_fecha( self._leer_t_tmp() )
            # print( 't_guardado = ', t_guardado )
            if t_guardado > t_compara:
                t_compara = t_guardado

        salida = []
        for notif in notificaciones:
            if notif.type == 'mention':
                creacion = a_zona( notif.created_at )
                # print( creacion, ' ---> ', self.cuerpo_notif( notif ), ' y comparando con ', t_compara )
                if self._format_fecha( creacion ) > t_compara:
                    salida.append( notif )

        self._guardar_tmp( self._cad2fecha( horaf ) )

        return salida

    def toot_texto( self, texto, en_respuesta_a = '', visibilidad = 'public' ):
        # Opciones de visibilidad: direct, private, unlisted, public
        if len( texto ) > MASTODON_MAX:
            texto = texto[ 0 : MASTODON_MAX - 1 ]
        if en_respuesta_a:
            self.mastodon.status_post( texto, in_reply_to_id = en_respuesta_a, visibility = visibilidad )
            print( 'toot_texto() => Tooteado \'', texto, '\' con visibilidad ', visibilidad, ' en respuesta a ', en_respuesta_a, ' (', self.mi_nombre(), ')' )
        else:
            self.mastodon.status_post( texto, visibility = visibilidad )
            print( 'toot_texto() => Tooteado \'', texto, '\' con visibilidad ', visibilidad, ' (', self.mi_nombre(), ')' )

    def cuerpo_notif( self, notificacion ):
        # La API de Mastodon API devuelve el contenido del toot en HTML
        doc = html.document_fromstring( notificacion['status']['content'] )
        # Preserve end-of-lines
        # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
        for br in doc.xpath("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"
        for p in doc.xpath("*//p"):
            p.tail = "\n" + p.tail if p.tail else "\n"
        return doc.text_content()

    def solicitante_notif( self, notificacion ):
        return notificacion['account']['acct']

    def id_notif( self, notificacion ):
        return notificacion['status']['id']

    def visibilidad_notif( self, notificacion ):
        return notificacion['status']['visibility']

    def lista_menciones_notif( self, notificacion ):
        return notificacion['status']['mentions']

    def mi_nombre( self ):
        return self.mastodon.account_verify_credentials()['username']

    def citados( self, notificacion, cabecera = '' ):
        mi_nombre = self.mi_nombre()
        lista_menciones = self.lista_menciones_notif( notificacion )
        cad_final = ''
        for elemento in lista_menciones:
            if elemento['acct'] != mi_nombre:
                cad_final = cad_final + ' @' + elemento['acct']
        cad_final = cad_final.strip()
        if cad_final != '':
            if cabecera !='':
                cad_final = cabecera + '\n' + cad_final
        else:
            cad_final = cabecera
        return cad_final

    def subir_media( self, foto ):
        print( 'toot_mensaje() => URL = ', foto['url'] )
        print( 'toot_mensaje() => REMOTE URL = ', foto['remote_url'] )
        print( 'toot_mensaje() => PREVIEW URL = ', foto['preview_url'] )
        print( 'toot_mensaje() => TIPO = ', foto['type'] )

        img_local = requests.get( foto.url ).content
        if foto.url:
            fich_url = foto.url
        else:
            fich_url = foto.remote_url
        mi_magic = magic.Magic( mime = True )
        tipo_mime = mi_magic.from_buffer( img_local )
        print( 'toot_mensaje() => TIPO MIME = ', tipo_mime )
        foto_subida = self.mastodon.media_post( img_local, mime_type = tipo_mime )
        return foto_subida

    def toot_mensaje( self, texto, ocultar_foto, fotos, id_mensaje = '' ):
        lista_fotos = []
        for foto in fotos:
            nueva_foto = self.subir_media( foto )
            lista_fotos.append( nueva_foto )
        
        if id_mensaje != '':
            mi_toot = self.mastodon.status_post( texto, sensitive = ocultar_foto, media_ids = lista_fotos, idempotency_key = id_mensaje )
        else:
            mi_toot = self.mastodon.status_post( texto, sensitive = ocultar_foto, media_ids = lista_fotos )
